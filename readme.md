## Sobre o Projeto

O projeto conta com dois tipos de cadastro, o de módulos 
e o de atividades. Para que se cadastre uma atividade, 
é necessário cadastrar previamente um módulo. Para acessar os formulários, 
o projeto conta com um menu de navegação, localizado no lado superior esquerdo.
<p>Ao acessar o formulário de cadastro, seja o de módulos ou o de atividades, 
do lado direito será exibido uma tabela, onde será listado tudo oque foi cadastrado,
possibilitando a alteração das informações ou exclusão de um determinado registro.</p>




## Instruções
- Após o download, execute o comando : composer install
- Instale as dependências : npm install
- Crie um banco de dados e configure no arquivo .env
- Gere as tabelas com o comando : php artisan migrate
- Gere a chave com o comando : php artisan key:generate
- Rode o serviço com o comando: php artisan serve
