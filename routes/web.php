<?php

// Por padrão exibe o formulário de cadastro de modulos
Route::get('/', 'ModulosController@index');


// Retorna também o cadastro de módulos
Route::get('/modulos', 'ModulosController@index');

// Dados de um módulo especifico
Route::get('/modulos/{id}', 'ModulosController@show');

// Insere um novo módulo no banco
Route::post('/modulos', 'ModulosController@store');

// Update de um módulo
Route::patch('/modulos/{id}', 'ModulosController@update');

// Delete de um módulo
Route::delete('/modulos/{id}', 'ModulosController@destroy');





// Cadastro de Atividades e retorno dos módulos cadastrados
Route::get('/atividades', 'AtividadesController@index');

// Dados de uma atividade especifica
Route::get('/atividades/{id}', 'AtividadesController@show');

// Insere uma nova atividade no banco
Route::post('/atividades', 'AtividadesController@store');

// Update de uma atividade
Route::patch('/atividades/{id}', 'AtividadesController@update');

// Delete de uma atividade
Route::delete('/atividades/{id}', 'AtividadesController@destroy');