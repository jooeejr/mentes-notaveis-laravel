<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model
{
    function modulos()
    {
        return $this->belongsTo('App\Modulo', 'modulo_id');
    }
}
