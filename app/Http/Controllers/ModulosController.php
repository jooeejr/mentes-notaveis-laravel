<?php

namespace App\Http\Controllers;

use App\Atividade;
use Illuminate\Http\Request;
use App\Modulo;

class ModulosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Modulo[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $modulos = Modulo::paginate('5');

        return view('modulos', compact('modulos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Mensagem dos campos obrigatórios
        $msg = [
            'titulo.required' => 'O campo título é obrigatório',
            'status.required' => 'O campo status é obrigatório',
            'descricao.required' => 'O campo descrição é obrigatório'
        ];


        // Validação dos campos obrigatórios
        $request->validate([
            'titulo' => 'required',
            'status' => 'required',
            'descricao' => 'required'
        ], $msg);


        // instancia da classe de modulos
        $modulo = new Modulo();


        // insere no banco os requests
        $modulo->titulo = $request->input('titulo');
        $modulo->status = $request->input('status');
        $modulo->descricao = $request->input('descricao');

        // Salva no banco
        $modulo->save();

        // Redireciona para o formulário novamente
        return redirect('/modulos');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // retorna os dados do modulo requisitado
        $modulo = Modulo::find($id);

        // Retorna o JSON
        return $modulo->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // instância do modelo
        $modulos = Modulo::find($id);

        // Atualiza os dados
        $modulos->titulo = $request->input('titulo');
        $modulos->descricao = $request->input('descricao');
        $modulos->status = $request->input('status');

        // Salva a atualização
        $modulos->save();

        return redirect('/modulos');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Remove os modulos das atividades
        $atividades = Atividade::where('modulo_id', $id);
        if($atividades)
        {
            $atividades->update(['modulo_id' => NULL]);
        }

        // encontra e deleta o modulo
        Modulo::find($id)->delete();

        return redirect('/modulos');
    }
}
