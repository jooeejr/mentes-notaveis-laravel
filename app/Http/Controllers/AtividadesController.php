<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modulo;
use App\Atividade;

class AtividadesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // retorna os módulos ativos para a tela de cadastro das atividades
        $modulos = Modulo::where('status', '1')->get();

        // Retorna as ativiades cadastradas
        $atividades = Atividade::with('modulos')->get();


        return view('atividades', compact('modulos', 'atividades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Mensagem dos campos obrigatórios
        $msg = [
            'titulo.required' => 'O campo título é obrigatório',
            'status.required' => 'O campo status é obrigatório',
            'modulo.required' => 'O campo módulo é obrigatório',
            'descricao.required' => 'O campo descrição é obrigatório'
        ];


        // Validação dos campos obrigatórios
        $request->validate([
            'titulo' => 'required',
            'status' => 'required',
            'modulo' => 'required',
            'descricao' => 'required'
        ], $msg);


        // instancia da classe de atividades
        $atividades = new Atividade();


        // insere no banco os requests
        $atividades->titulo = $request->input('titulo');
        $atividades->status = $request->input('status');
        $atividades->descricao = $request->input('descricao');
        $atividades->modulo_id = $request->input('modulo');

        // Salva no banco
        $atividades->save();

        // Redireciona para o formulário novamente
        return redirect('/atividades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // retorna os dados do modulo requisitado
        $atividade = Atividade::find($id);

        // Retorna o JSON
        return $atividade->toJson();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // instância do modelo
        $atividades = Atividade::find($id);

        // Atualiza os dados
        $atividades->titulo = $request->input('titulo');
        $atividades->descricao = $request->input('descricao');
        $atividades->status = $request->input('status');
        $atividades->modulo_id = $request->input('modulo');

        // Salva a atualização
        $atividades->save();

        return redirect('/atividades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // encontra e deleta o modulo
        Atividade::find($id)->delete();

        return redirect('/atividades');
    }
}
