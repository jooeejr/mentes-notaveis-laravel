// Edição de Módulo

function editModulo(id)
{
    $.getJSON("/modulos/"+id,function (data)
        {
            // Seta o resultado na modal
            $("#titulo-att").val(data.titulo);
            $("#status-att").val(data.status);
            $("#descricao-att").val(data.descricao);

            // Altera o action do form
            $("#formEdicaoModulo").attr('action', '/modulos/'+data.id);

            // Exibe a modal
            $('#editModulos').modal('show');
        }
    );


}


function deletaModulo(id)
{
    swal({
       title: 'Deseja deletar este módulo ?',
        icon: "warning",
       buttons: ['Não', 'Sim']
    }).then((willDelete) => {
        if (willDelete) {

           // Confirma e deleta o módulo
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

           $.ajax({
              url: 'modulos/'+id,
              type: 'post',
               data: {_method: 'delete'},
               success: function(data)
               {
                window.location.reload();
               }
           });
        }
    });
}




// Edição de atividades
function editAtividade(id)
{
    $.getJSON("/atividades/"+id,function (data)
        {
            // Seta o resultado na modal
            $("#titulo-att").val(data.titulo);
            $("#status-att").val(data.status);
            $("#descricao-att").val(data.descricao);
            $("#modulo-att").val(data.modulo_id);

            // Altera o action do form
            $("#formEdicaoAtividade").attr('action', '/atividades/'+data.id);

            // Exibe a modal
            $('#editAtividades').modal('show');
        }
    );


}


function deletaAtividade(id)
{
    swal({
        title: 'Deseja deletar esta atividade ?',
        icon: "warning",
        buttons: ['Não', 'Sim']
    }).then((willDelete) => {
        if (willDelete) {

            // Confirma e deleta o módulo
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                url: 'atividades/'+id,
                type: 'post',
                data: {_method: 'delete'},
                success: function(data)
                {
                    window.location.reload();
                }
            });
        }
    });
}