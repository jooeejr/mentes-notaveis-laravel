
{{--Extende o layout base da aplicação--}}
@extends('layout.layout', ["current" => "atividades"])

{{--Cadastro de Módulos--}}
@section('corpo')

    {{--Modal da Módulos--}}
    @component('components.modal-atividades', ['modulos' => $modulos])

    @endcomponent

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                &nbsp;
            </div>

            <div class="col-md-4">
                <fieldset>
                    <div class="col-md-12">
                        <legend>Cadastro de Atividades</legend>
                        <hr>
                    </div>
                    <form method="post" action="/atividades">
                        @csrf
                        <div class="col-md-12">
                            <input required name="titulo" type="text" class="form-control {{ ($errors->has('titulo')) ? 'is-invalid' : '' }}" placeholder="Título">

                            @if($errors->has('titulo'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('titulo') }}
                                </div>
                            @endif
                        </div>



                        <div class="col-md-12">
                            &nbsp;
                        </div>

                        <div class="col-md-12">
                            <select required name="status" class="form-control {{ ($errors->has('status')) ? 'is-invalid' : '' }}">
                                <option value="">Status...</option>
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>

                            @if($errors->has('status'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12">
                            &nbsp;
                        </div>

                        <div class="col-md-12">
                            <select required name="modulo" class="form-control {{ ($errors->has('modulo_id')) ? 'is-invalid' : '' }}">
                                <option value="">Selecione o módulo...</option>
                                @foreach($modulos AS $mod)
                                    <option value="{{ $mod->id }}">{{ $mod->titulo }}</option>
                                @endforeach
                            </select>

                            @if($errors->has('modulo_id'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('modulo_id') }}
                                </div>
                            @endif
                        </div>



                        <div class="col-md-12">
                            &nbsp;
                        </div>


                        <div class="col-md-12">
                            <textarea required placeholder="Descrição da atividade" class="descricaoModulo form-control {{ ($errors->has('descricao')) ? 'is-invalid' : '' }}" name="descricao"></textarea>

                            @if($errors->has('descricao'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('descricao') }}
                                </div>
                            @endif
                        </div>

                        <div class="col-md-12">
                            &nbsp;
                        </div>

                        <div class="col-md-12">
                            <button class="btn btn-primary form-control btnCadastrarModulo">Cadastrar</button>
                        </div>
                    </form>
                </fieldset>
            </div>

            <div class="col-md-1">
                &nbsp;
            </div>

            <div class="col-md-7">
                <div class="col-md-12">
                    <legend>Atividades Cadastradas</legend>
                    <hr>
                </div>
                <table class="table table-striped">
                    <thead>
                    <th>ID</th>
                    <th>Título</th>
                    <th>Módulo</th>
                    <th>Status</th>
                    <th>Editar</th>
                    <th>Deletar</th>
                    </thead>

                    <tbody>
                    @foreach($atividades AS $atv)
                        <tr>
                            <td>{{ $atv->id }}</td>
                            <td>{{ $atv->titulo }}</td>
                            <td>{{ $atv->modulos['titulo'] }}</td>
                            <td>{{ ($atv->status == 1) ? 'Ativo' : 'Inativo' }}</td>
                            <td>
                                <button onClick="editAtividade({{ $atv->id }})" type="button" class="btn btn-warning">
                                    <i class="fas fa-edit"></i>
                                </button>
                            </td>

                            <td>
                                <button onClick="deletaAtividade({{ $atv->id }})" type="button" class="btn btn-danger">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>



            </div>
        </div>
    </div>
@endsection