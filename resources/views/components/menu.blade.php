<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link {{ ($current == 'modulos') ? "active" : "" }}" href="/modulos">Cadastro de Módulos</a>
            <a class="nav-item nav-link {{ ($current == 'atividades') ? "active" : "" }}" href="/atividades">Cadastro de Atividades</a>
        </div>
    </div>
</nav>