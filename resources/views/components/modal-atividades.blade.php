<!-- Modal -->

<div class="modal fade" id="editAtividades" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edição de Atividade</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formEdicaoAtividade" method="post" action="">
                <div class="modal-body">
                    @csrf
                    @method('patch')
                    <div class="col-md-12">
                        <input required id="titulo-att" name="titulo" type="text" class="form-control {{ ($errors->has('titulo')) ? 'is-invalid' : '' }}" placeholder="Título">

                        @if($errors->has('titulo'))
                            <div class="invalid-feedback">
                                {{ $errors->first('titulo') }}
                            </div>
                        @endif
                    </div>



                    <div class="col-md-12">
                        &nbsp;
                    </div>


                    <div class="col-md-12">
                        <select required id="modulo-att" name="modulo" class="form-control {{ ($errors->has('modulo_id')) ? 'is-invalid' : '' }}">
                            <option value="">Selecione o módulo...</option>
                            @foreach($modulos AS $mod)
                                <option value="{{ $mod->id }}">{{ $mod->titulo }}</option>
                            @endforeach
                        </select>

                        {{--@if($errors->has('modulo_id'))--}}
                            {{--<div class="invalid-feedback">--}}
                                {{--{{ $errors->first('modulo_id') }}--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    </div>

                    <div class="col-md-12">
                        &nbsp;
                    </div>


                    <div class="col-md-12">
                        <select required name="status" id="status-att" class="form-control {{ ($errors->has('status')) ? 'is-invalid' : '' }}">
                            <option value="">Status...</option>
                            <option value="1">Ativo</option>
                            <option value="0">Inativo</option>
                        </select>

                        @if($errors->has('status'))
                            <div class="invalid-feedback">
                                {{ $errors->first('status') }}
                            </div>
                        @endif
                    </div>

                    <div class="col-md-12">
                        &nbsp;
                    </div>


                    <div class="col-md-12">
                        <textarea required id="descricao-att" placeholder="Descrição do Módulo" class="descricaoModulo form-control {{ ($errors->has('descricao')) ? 'is-invalid' : '' }}" name="descricao"></textarea>

                        @if($errors->has('descricao'))
                            <div class="invalid-feedback">
                                {{ $errors->first('descricao') }}
                            </div>
                        @endif
                    </div>

            </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Salvar Alterações</button>
                </div>
            </form>

        </div>
    </div>
</div>