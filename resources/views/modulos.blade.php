
{{--Extende o layout base da aplicação--}}
@extends('layout.layout', ["current" => "modulos"])



{{--Cadastro de Módulos--}}
@section('corpo')

    {{--Modal da Módulos--}}
    @component('components.modal-modulos')
    @endcomponent

    <div class="container-fluid">
        <div class="row">

            <div class="col-md-12">
                &nbsp;
            </div>

            <div class="col-md-4">
                <fieldset>
                        <div class="col-md-12">
                            <legend>Cadastro de Módulos</legend>
                            <hr>
                        </div>
                        <form method="post" action="/modulos">
                            @csrf
                            <div class="col-md-12">
                                <input name="titulo" type="text" class="form-control {{ ($errors->has('titulo')) ? 'is-invalid' : '' }}" placeholder="Título">

                                @if($errors->has('titulo'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('titulo') }}
                                    </div>
                                @endif
                            </div>



                            <div class="col-md-12">
                                &nbsp;
                            </div>

                            <div class="col-md-12">
                                <select name="status" class="form-control {{ ($errors->has('status')) ? 'is-invalid' : '' }}">
                                    <option value="">Status...</option>
                                    <option value="1">Ativo</option>
                                    <option value="0">Inativo</option>
                                </select>

                                @if($errors->has('status'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('status') }}
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                &nbsp;
                            </div>


                            <div class="col-md-12">
                                <textarea placeholder="Descrição do Módulo" class="descricaoModulo form-control {{ ($errors->has('descricao')) ? 'is-invalid' : '' }}" name="descricao"></textarea>

                                @if($errors->has('descricao'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('descricao') }}
                                    </div>
                                @endif
                            </div>

                            <div class="col-md-12">
                                &nbsp;
                            </div>

                            <div class="col-md-12">
                                <button class="btn btn-primary form-control btnCadastrarModulo">Cadastrar</button>
                            </div>
                        </form>
                    </fieldset>
            </div>

            <div class="col-md-1">
                &nbsp;
            </div>

            <div class="col-md-7">
                <div class="col-md-12">
                    <legend>Módulos Cadastrados</legend>
                    <hr>
                </div>
                <table class="table table-striped">
                    <thead>
                        <th>ID</th>
                        <th>Título</th>
                        <th>Status</th>
                        <th>Editar</th>
                        <th>Deletar</th>
                    </thead>

                    <tbody>
                        @foreach($modulos AS $mod)
                            <tr>
                                <td>{{ $mod->id }}</td>
                                <td>{{ $mod->titulo }}</td>
                                <td>{{ ($mod->status == 1) ? 'Ativo' : 'Inativo' }}</td>
                                <td>
                                    <button onClick="editModulo({{ $mod->id }})" type="button" class="btn btn-warning">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                </td>

                                <td>
                                    <button onClick="deletaModulo({{ $mod->id }})" type="button" class="btn btn-danger">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="col-md-12">
                    {{ $modulos->links() }}
                </div>
            </div>

        </div>
    </div>
@endsection