<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    {{--Jquery--}}
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    {{--Bootstrap--}}
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>

    {{--Sweet Alert--}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


    {{--Estilos--}}
    <link rel="stylesheet" href="{{ asset('css/estilo.css') }}">

    {{--JS--}}
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>

    {{--icones--}}
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">



    <title>Mentes Notáveis - Avaliação</title>
</head>
<body>

    {{--Menu Superior--}}
    @component('components.menu', ['current' => $current])
    @endcomponent

    {{--Corpo da aplicação--}}
    @hasSection('corpo')
        @yield('corpo')
    @endif

</body>
</html>
